package com.weixin.gong.http;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;

import java.net.URI;

/**
 * @author weixin.gong
 * @date 17-1-22 下午8:21
 */
public class Request {

    private final String methodName;
    private final URI uri;

    private Request(final String methodName, final URI uri) {
        this.methodName = methodName;
        this.uri = uri;
    }

    public static Request Get(final URI uri) {
        return new Request(HttpGet.METHOD_NAME, uri);
    }

    public static Request Get(final String uri) {
        return Get(URI.create(uri));
    }

    public static Request Post(final URI uri) {
        return new Request(HttpPost.METHOD_NAME, uri);
    }

    public static Request Post(final String uri) {
        return Post(URI.create(uri));
    }

    public static Request Put(final URI uri) {
        return new Request(HttpPut.METHOD_NAME, uri);
    }

    public static Request Put(final String uri) {
        return Put(URI.create(uri));
    }
}
